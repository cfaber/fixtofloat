#include <stdio.h>
#include <stdlib.h>
#include "../include/utils.h"
#include <math.h>
//#include "../include/fix.h"
			
int readBinFile_16(FILE* inp, long int *inp_size, fixPt16** src, size_t* bufLen, int run_num){
	//printf("Entering read\n");
	if (fseek(inp, 0L, SEEK_END) != 0){
		printf("Something has gone wrong.\n");
		return 0;	
	}
	else{
		/* return current file position after fseek to effectively get 
		 * # of bytes */
		*inp_size = ftell(inp);
		if ((*inp_size) == -1){
			printf("Something has gone wrong.\n");
			return 0;
		}

		rewind(inp);
		*src = (fixPt16*) malloc( sizeof(fixPt16) * ((*inp_size) + 1) );
		*bufLen = fread(*src, sizeof(fixPt16), *inp_size, inp);
		if (ferror(inp)){
			printf("Something has gone wrong.\n");
			return 0;
		}		
	}
	return 1;
}

int printFixFile_16(fixPt16* src, size_t bufLen, QValue Q, int run_num){
	FILE *outp;
	char fileName[50];
	int16_t whole;
	uint16_t frac;
	int j;

	sprintf(fileName, "./data/output/fixedpoint%i.csv", run_num);
	
	outp = fopen(fileName, "w");

	fprintf(outp, "Number,Value\n");
	printf("%s\n", fileName );
	//Calculate fixedpoint values
	for(j = 0; j < bufLen; ++j){
		//Decimal
		if(src[j] & 0x80000000){
			whole = ((src[j] >> Q));
			++whole;
		}
		else{
			whole = (src[j] >> Q);
		}
		//Fractional 
		frac = (uint16_t) src[j] << (16-Q);
		frac = (uint16_t)frac >> (16-Q);
		if(whole <= 0){
			frac = (1-(frac/(pow(2,Q))))*10000;
		}
		else{
			frac = (frac / (pow(2,Q)))*10000;
		}
		fprintf(outp, "%u,%i.%i\n", j, whole, frac);
	}
	fclose(outp);
	return 1;
}

int printFixFile_32(fixPt32* src, size_t bufLen, QValue Q, int run_num)
{
	FILE *outp;
	int32_t whole;
	uint32_t frac;
	//float tst;
	int j;
	outp = fopen("./fixedpoint1.csv", "w");
	fprintf(outp, "Number,Value\n");
	//Calculate fixedpoint values
	for(j = 0; j < bufLen; ++j){
		//tst = ( ((float) (src[j])) / ((float) (1 << (Q))) );
		//Decimal
		if(src[j] & 0x8000000000000000){
			whole = ((src[j] >> Q));
			++whole;
		}
		else{
			whole = (src[j] >> Q);
		}
		//Fractional 
		frac = (uint32_t) src[j] << (32-Q);
		frac = (uint32_t)frac >> (32-Q);
		if(whole <= 0){
			frac = (1-(frac/(pow(2,Q))))*1000000;
		}
		else{
			frac = (frac / (pow(2,Q)))*1000000;
		}
		fprintf(outp, "%u,%i.%i\n", j, whole, frac);
		//fprintf(outp, "TST: %f\n", tst);
	}
	fclose(outp);
	return 1;
}


int readBinFile_32(FILE* inp, long int *inp_size, fixPt32** src, size_t* bufLen,int run_num)
{
	printf("Entering read\n");
	if (fseek(inp, 0L, SEEK_END) != 0){
		printf("Something has gone wrong.\n");
		return 0;	
	}
	else{
		*inp_size = ftell(inp);
		if ((*inp_size) == -1){
			printf("Something has gone wrong.\n");
			return 0;
		}
		rewind(inp);
		*src = (fixPt32*) malloc( sizeof(fixPt32) * ((*inp_size) + 1) );
		*bufLen = fread(*src, sizeof(fixPt32), *inp_size, inp);
		if (ferror(inp)){
			printf("Something has gone wrong.\n");
			return 0;
		}
				
	}
	return 1;
}

int createFloatBuff(float **buf, size_t bufLen)
{
	*buf = (float*)malloc( sizeof(float) * (bufLen));
	return 1;
}

int printFloatFiles(FILE* outp, float *src, size_t bufLen, int run_num){
	FILE* outp_csv;
	char fileName [50];
	long unsigned int i;
	sprintf(fileName, "./data/output/floatpoint%i.csv", run_num);
	outp_csv = fopen(fileName, "w");
	fprintf(outp_csv, "Number,Value\n");
	for(i = 0; i < bufLen; ++i){
		fwrite(&(src[i]), sizeof(float), 1, outp);
		fprintf(outp_csv, "%li,%f\n", i, src[i]);
	}
	fclose(outp_csv);
	return 1;
}

int spaceForTimingData(int d, int r, int c, clock_t**** timingdata)
{
	int i, j, rd, rc, rdc;
	*timingdata = (clock_t ***) malloc(d * sizeof(clock_t **));
	rd = r * d;
	(*timingdata)[0] = (clock_t **) malloc(rd * sizeof(clock_t *));
	rdc = rd * c;
	(*timingdata)[0][0] = (clock_t *) malloc(rdc * sizeof(clock_t));
	rc = r * c;
	for (i = 0; i < d; ++i){
		(*timingdata)[i] = (*timingdata)[0] + r * i;
		for (j = 0; j < r; ++j){ 
			(*timingdata)[i][j] = (*timingdata)[0][0] + (rc * i) + (c * j);
		}
	}

	return 0;

}


int printTimingData(int d, int r, int c, clock_t*** timingdata){
	FILE* outp_csv;
	int i, k;
	outp_csv = fopen("TimingData.csv", "w");
	fprintf(outp_csv,"RUN: #\n");
	fprintf(outp_csv,"Conversions, start, end\n");
	fprintf(outp_csv,"-----------------------\n\n");
	for(k = 0; k < d; ++k ){
		fprintf(outp_csv,"RUN: %i\n", k );
		for(i = 0; i < r; ++i ){
			fprintf(outp_csv,"%i, %ju, %ju\n", i, timingdata[k][i][0], timingdata[k][i][1]);
		}
		fprintf(outp_csv,"-----------------------\n\n");
	}
	fclose(outp_csv);
	return 0;
}