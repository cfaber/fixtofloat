//FLoat to fix first draft
//	Clayton Faber


#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
#include "fix.h"
#include "utils.h"
//#include "hdf5.h"


#define NUM_OF_RUNS 1
#define NUM_OF_COLS 2
#define Statistic

//#include <string.h>

int main(int argc, char* argv[]){
	FILE *inp, *outp;
	QValue QVal;
	fixPt16 *src_fixPt16 = 0;
	char outp_name[60];
	//fixPt32 *src32 = 0;
	float *floatbuf ;
	long int inp_size;
    long unsigned int i;
    int k;
    int r, c, d;			
	size_t bufLen; 				//number of bytes read into source
	clock_t*** timingdata = 0;  //3D buffer for the timing data

	

	if (argc != 3){
		printf("Usage:  Interperates a binary file as fixed point values whoes Q\n");
		printf("		value is defined on input. The fixed point values are then\n");
		printf("		written to an output binary file as floating point numbers\n");
		printf("Example: ./FixToFloat input.bin Q\n\n");
    	return(0);
  	}
	QVal = (uint32_t)atoi(argv[2]);
	if(QVal <= 0 || QVal >= 32){ 
		printf("ERROR: Qvalue Invalid\n"); 
		return -1;
	}
	inp = fopen(argv[1], "rb");
	if(!inp){
		printf("ERROR: FILE NOT FOUND\n"); 
		return -1;
	}
	if( !(readBinFile_16(inp, &inp_size, &src_fixPt16, &bufLen, 0)) ){
		printf("ERROR: File load \n");
		return -1;
	}

	
	if(!(printFixFile_16(src_fixPt16, bufLen, QVal, 0))){
		printf("ERROR: CAN NOT CREATE FIXED POINT CSV");
		return -1;
	}
	d = NUM_OF_RUNS;
	r = (bufLen / 1000) + 1;
	c = NUM_OF_COLS;

	if (spaceForTimingData(d, r, c, &timingdata) != 0)
	{
		printf("Something went wrong with spaceForTimingData allocation.\n");
		return -1;
	}
	//Start Timing stuff here
	for(k = 0; k < NUM_OF_RUNS; ++k){
		
		sprintf(outp_name, "./data/output/output%i.bin", k);
		outp = fopen(outp_name, "wb");
		if(!outp){
			printf("ERROR: CAN NOT CREATE FILE\n"); 
			return -1;
		}
		
		createFloatBuff(&floatbuf, bufLen);

		// The actual conversions //
		for(i = 0; i < bufLen; ++i){
			  
			//Start conversion - Time per 1000 conversion
			if (i % 1000 == 0) timingdata[k][i/1000][NUM_OF_COLS-NUM_OF_COLS] = clock();
			//Do Conversion
			floatbuf[i] = fixPt16_toFloat(src_fixPt16[i], QVal);
			//End conversion - Time per 1000 conversions
			if (i % 1000 == 0) timingdata[k][i/1000][NUM_OF_COLS - 1] = clock();

		}
		printf("Run #%i completed\n", (k+1));
		printf("Writing data and freeing memory\n");
		printFloatFiles(outp, floatbuf, bufLen, k);
		free(floatbuf);
		fclose(outp);
	}
	printf("\nPrinting timing data and clearing memory\n");
	printTimingData(d, r, c, timingdata);

	fclose(inp);
	
    free(timingdata[0][0]);
    free(timingdata[0]);
    free(timingdata);

	return 0;
}


