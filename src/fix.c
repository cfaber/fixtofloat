#include "fix.h"

float fixPt16_toFloat(fixPt16 op1, QValue Q){
	return ( ((float)(op1)) / ((float)(1 << Q)) );
}

float fixPt32_toFloat(fixPt32 op1, QValue Q){
	return  ( ((float)(op1)) / ((float)(1 << Q)) );	
}