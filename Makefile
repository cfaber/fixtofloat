# Makefile
SRC := ./src

C_FILES := $(SRC)/FixToFloat.c
C_FILES += $(SRC)/utils.c 
C_FILES += $(SRC)/fix.c


CC := gcc

# Linker
#LD := $(CROSS_COMPILE)gcc
#STRIP := $(CROSS_COMPILE)strip

# Important to have hard option!

#C_FLAGS += -O2
C_FLAGS := -Wall
C_FLAGS += -I ./include


# Libraries 
L_LIBS := -lm
#L_LIBS += -lhdf5
#L_LIBS += -lhdf5_hl 

# Recipe for C code

O_FILES := $(C_FILES:.c=.o)

# ----------------------------------------------

all: 	FixToFloat

FixToFloat:	$(O_FILES)
	$(CC) -static -o $@ $(O_FILES) $(L_FLAGS) $(L_LIBS)

%.o : %.c
	$(CC) $(C_FLAGS) -c -o $@ $< $(L_FLAGS) $(L_LIBS)


.PHONY	: clean all
clean	:
	-rm -f $(O_FILES)
	-rm -f FixToFloat
