#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
#include "fix.h"


int readBinFile_16(FILE* inp, long int *inp_size, fixPt16** src, size_t* bufLen, int run_num);
int printFixFile_16(fixPt16* src, size_t bufLen, QValue Q, int run_num);
int readBinFile_32(FILE* inp, long int *inp_size, fixPt32** src, size_t* bufLen, int run_num);
int printFixFile_32(fixPt32* src, size_t bufLen, QValue Q, int run_num);

int createFloatBuff(float **buffer, size_t bufLen);
int printFloatFiles(FILE* outp, float *src, size_t bufLen, int run_num);

int spaceForTimingData(int d, int r, int c, clock_t**** timingdata);
int printTimingData(int d, int r, int c, clock_t*** timingdata);
