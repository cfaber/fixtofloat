// fix.h
// Clayton Faber
// Header file for fixed point operations

#include <stdio.h>
#include <stdlib.h>	
#include <stdint.h>
#include <time.h>

typedef int16_t 		 fixPt16;		
typedef int32_t			 fixPt32;
typedef int64_t 		 fixPt64;
typedef	uint32_t 		 QValue;

//#define Q			12
//#define twoQ		24

// Convert a fixed-point number to a float
float fixPt16_toFloat(fixPt16 op1, QValue Q);
float fixPt32_toFloat(fixPt32 op1, QValue Q);	   

