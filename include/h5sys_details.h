//
// sysdetails.h
//
// Author:			Anthony Cabrera
// Date:			8/14/16
// Description:		Contains h5 attribute names to include in the timing tests.
//					Attributes included are:
//						processor name, processor speed, number of processors,
//						total number of cores, L2 Cache (per core), L3 cache, 
//						memory
//	
//					

#ifndef H5SYS_DETAILS_H
#define H5SYS_DETAILS_H

#include <stdio.h>

#define NUM_ATTR	7	

typedef struct h5Attributes h5Attributes;

// I was too lazy to implement a linked list type structure. Maybe later.
/*struct h5Attributes 
{
	char 			attribute_key[32];
	char 			attribute_val[32];
	h5Attributes*	next;
}*/ 

char** h5sys_names();
char** h5sys_vals();



#endif /* H5SYS_DETAILS_H */